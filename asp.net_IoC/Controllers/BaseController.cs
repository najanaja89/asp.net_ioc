﻿using asp.net_IoC.Modules;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace asp.net_IoC.Controllers
{
    public class BaseController: Controller
    {
        protected IKernel kernelLanguage;
        public BaseController()
        {
            kernelLanguage = new StandardKernel(new LanguageModule());
        }
    }
}