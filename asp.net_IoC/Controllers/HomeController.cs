﻿using asp.net_IoC.Models;
using asp.net_IoC.Modules;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace asp.net_IoC.Controllers
{
    public class HomeController : Controller //BaseController
    {

        readonly ILanguage language;

        public HomeController(ILanguage language)
        {
            this.language = language;
        }
        public ActionResult Index()
        {
            //var language = kernelLanguage.Get<ILanguage>();
            ViewBag.Language = language.Message();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}