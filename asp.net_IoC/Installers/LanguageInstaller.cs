﻿using asp.net_IoC.Models;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.net_IoC.Installers
{
    public class LanguageInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                .For<ILanguage>()
                .ImplementedBy<EnglishLanguage>()
                .LifestyleTransient());
        }
    }
}