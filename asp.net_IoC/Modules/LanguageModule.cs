﻿using asp.net_IoC.Models;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.net_IoC.Modules
{
    public class LanguageModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILanguage>().To<RussianLanguage>();
        }
    }
}