﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp.net_IoC.Models
{
    public interface ILanguage
    {
        string Message();
    }
}
